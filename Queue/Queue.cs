﻿using System.Collections;
using System.Diagnostics;

namespace Queue;

public class Node<T>
{
    public Node(T data)
    {
        Data = data;
    }
    public T Data { get; }
    public Node<T>? Next { get; set; }
}

public class Queue<T> : IEnumerable<T>
{
    private Node<T>? _head;
    private Node<T>? _tail;

    public void Enqueue(T data)
    {
        var node = new Node<T>(data);
        var tempNode = _tail;
        _tail = node;
        if (Count == 0)
            _head = _tail;
        else
        {
            Debug.Assert(tempNode != null, nameof(tempNode) + " != null");
            tempNode.Next = _tail;
        }
        Count++;
    }

    public T Dequeue()
    {
        if (Count == 0)
            throw new InvalidOperationException();
        Debug.Assert(_head != null, nameof(_head) + " != null");
        var output = _head.Data;
        _head = _head.Next;
        Count--;
        return output;
    }

    public T First
    {
        get
        {
            if (IsEmpty)
                throw new InvalidOperationException();
            Debug.Assert(_head != null, nameof(_head) + " != null");
            return _head.Data;
        }
    }

    public T Last
    {
        get
        {
            if (IsEmpty)
                throw new InvalidOperationException();
            Debug.Assert(_tail != null, nameof(_tail) + " != null");
            return _tail.Data;
        }
    }

    private int Count { get; set; }
    private bool IsEmpty => Count == 0;

    public void Clear()
    {
        _head = null;
        _tail = null;
        Count = 0;
    }

    public bool Contains(T data)
    {
        var current = _head;
        while (current != null)
        {
            Debug.Assert(current.Data != null, "current.Data != null");
            if (current.Data.Equals(data))
                return true;
            current = current.Next;
        }
        return false;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable)this).GetEnumerator();
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
        var current = _head;
        while (current != null)
        {
            yield return current.Data;
            current = current.Next;
        }
    }
}

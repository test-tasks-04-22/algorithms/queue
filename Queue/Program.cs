﻿var queue = new Queue.Queue<string>();
queue.Enqueue("1");
queue.Enqueue("2");
queue.Enqueue("3");
queue.Enqueue("4");
 
Console.Write("Очередь после добавления элементов: ");
foreach (var item in queue)
    Console.Write($"{item} ");

var firstItem = queue.Dequeue();
Console.WriteLine($"\nИзвлеченный первый элемент: {firstItem}");
Console.WriteLine($"Первый элемент изменённой очереди: {queue.First}");
Console.WriteLine($"Последний элемент очереди: {queue.Last}");

Console.Write("Очередь после изменения: ");
foreach (var item in queue)
    Console.Write($"{item} ");
Console.WriteLine($", {queue.Count()} элементов");
    
var isPresent = queue.Contains("7");
Console.WriteLine(isPresent == true ? "7 присутствует" : "7 отсутствует");

queue.Clear();

Console.WriteLine(!queue.Any() ? "Элементы очереди удалены" : "Очередь не пуста");  
   